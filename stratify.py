"""
stratify.py: 
    
In a typical workflow, this module is run immediately after running the 
`prepare` module. Having ultimately derived the x-hourly accumulations data in
that module, `stratify` is then used to split the resulting x-hourly data 
(which up to this point is saved on file according to its 24-hour date), into 
the various convective regimes or months we are interested in, and concatenate 
all relevant data into a single dataarray, which is then saved to file.

This is achieved using the function `run_stratification`, which is the only
function in this module requiring user interaction. The stratifications to be
performed are specified in `config.STRAT_DICT`.
"""


import os
import pickle
import xarray as xr
import pandas as pd
from radclims.utils import open_pickles, date2subpath, paths2datelist
from radclims.config import STRAT_DICT


def regime_dates(root, regimes_path, regime_nums):
    """
    Gets all matching dates for a specific regime/month for which data is 
    available in `root`. Returns a pandas.DatetimeIndex containing all such 
    dates.
    
    Args:
        root (str): root directory (absolute path) containing year directories
            within which our pickled data lives.
        regimes_path (str): path to pickled regime dataarray.
        regime_nums (list of ints): regime numbers of interest. See
            `config.REGIMES_DICT` for a mapping of each regime/month to its
            associated integer key.
        
    Returns:
        regime_dates (pandas.DatetimeIndex): all dates for which there exists 
            an entry within the regime/month file (given by `regimes_path`).
    """
    # open regimes xarray.DataArray
    regimes = open_pickles(regimes_path)
    # get list of dates for which pickled data exists
    data_dates = paths2datelist(root)
    # reduce regimes to only regime numbers of interest
    regimes_data = []
    for num in regime_nums:
        regimes_data.append(regimes[regimes == num])
    # concatenate each of the regimes of interest together
    regimes_data = xr.concat(regimes_data, dim='date')
    # get intersecting dates
    regime_dates = (pd.DatetimeIndex(regimes_data.date.values) & data_dates)
    # return dates
    return regime_dates # NEED TO SORT


def stratify_by_date(root, dates):
    """
    Read all pickled data from root whose date is contained within `dates`, 
    concatenate into single xarray.DataArray and return result.
    
    Args:
        root (str): root directory containing yyyy/yyyymmdd sub-directories 
            within which the pickled data files lives.
        dates (list of datetime.date objects): the dates for which we want 
            to extract data.
        
    Returns:
        result (xarray.DataArray): concatenated dataarray containing all data 
            within `dates`, with spatial dimensions 'x' and 'y', and date
            dimension 'date'.
    """
    # get file path for each date in `dates`
    paths = [f'{root}/{date2subpath(date)}' for date in dates]
    # read in each file pickle path in `paths` (xarray.DataArray object), 
    # append to `arrays`
    arrays = []
    for path in paths:
        data = open_pickles(path)
        arrays.append(data)
    # combine `arrays` along date dim, return resulting xarray.DataArray
    result = xr.concat(arrays, dim='date') 
    return result


def stratify_by_regime(root, regimes_path, regime_nums):
    """
    Finds all pickled data within `root` which falls under the convective
    regime/month(s) given by `regime_nums`, concatenates data into a single 
    dataarray.
    
    Args:
        root (str): root directory containing yyyy/yyyymmdd sub-directories 
            within which the pickled data files lives.
        regimes_path (str): path to pickled regime/month dataarray.
        regime_nums (list of ints): regime numbers of interest. See
            `config.REGIMES_DICT` for a mapping of each regime/month to its
            associated integer key.
        
    Returns:
        data (xarray.DataArray): conacatenated dataarray containing all 
            available data whose date corresponds to the regime/month(s) given
            by `regime_nums`.
    """ 
    # dates for which data is present, correpsonding to `regime_num`
    dates = regime_dates(root, regimes_path, regime_nums)
    # pull out data for that regime, concatenate and return
    data = stratify_by_date(root, dates)    
    return data


def run_stratification(source_root, dest_root):
    """
    Runs `stratify_by_regime` over a prespecified configuration of regimes
    (given in config.STRAT_DICT), and writes the resulting dataarrays to file. 
    
    Args:
        source_root (str): location of the daily dataarrays to be stratified - 
            typically these are the daily aggregated x-hourly accumulation 
            dataarrays, as constructed by the `prepare` module.
        dest_root (str): the location to where the stratified dataarrays will 
            be written. This will typically be something like 
            .../data/darwin/accumulations/concatenated/3h or 24h (depending
            on which x-hourly data type is being stratified). This function will 
            construct a separate directory within `dest_root` for each 
            regime/month number, and the single resulting dataarray for that 
            number will be saved as a pickle file.
    """
    for cat in STRAT_DICT:
        content = STRAT_DICT[cat]
        # fundamental regime or month number(s) corresponding to regime `cat
        # and path to the regimes (or months) dataarray
        regime_nums, regimes_path = content
        # extract all data within source_root corresponding to this regime,
        # as sinlge dataarray
        data = stratify_by_regime(source_root, regimes_path, regime_nums)
        dest_path = f"{dest_root}/{cat}"
        # if the destination path deosn't already exist, make it
        if not os.path.isdir(dest_path):
            os.makedirs(dest_path)
        # write this data to the destination path    
        with open(f"{dest_path}/data.pick", "wb") as f:
            pickle.dump(data, f)