"""
plot.py: this module facilitates construction of the 'PoPX' and 'PrecipY' 
figures.
"""


import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from radclims.config import (
    FREQUENCY_DICT, COLOURMAP, RADAR_LAT, RADAR_LON, EXTENT, LAT_SCALE, 
    LON_SCALE, LOCS_TO_PLOT
)
from radclims.utils import int2str


class DataPlotSpecs:
    """
    Base class which extracts all relevant attributes from `data` before being
    plotted. This exists because an earlier iteration of the radclims package 
    offered other plot types also, which also depended on the attributes data 
    derived through this base class.
    
    Args:
        data (xarray.DataArray): the data to be plotted. This should be the 
        direct output of the function `data_composer` in `compose.py`.
    """
    def __init__(self, data):
        # put data into array format
        self.array = data.values
        self.values = np.ma.masked_where(np.isnan(self.array), self.array)
        # frequency of the data
        self.freq = data.frequency
        # put hours into array format
        self.hours = data.hour.values
        # put spatial coords into array format (assumes x and y are the same)
        self.coords = data.x.values
        # x and y coords to metres
        self.x = 1000 * data.x
        self.y = 1000 * data.y
        self.units = data.units


class SpatialPlot(DataPlotSpecs):
    """
    Plotting class used to construct the preset spatial 'PoPX' and 'PrecipY' 
    figures. The class itself does not allow user-modification of the figures
    in any way; however, this can be achieved through the relevant declared 
    variables in `config.py` if desired.
    
    Args:
        data (xarray.DataArray): the data to be plotted. This should be the 
        direct output of the function `data_composer` in `compose.py`.
    """
    def __init__(self, data):        
        DataPlotSpecs.__init__(self, data)
        # variable containing figure size
        self.size = FREQUENCY_DICT[self.freq]['figsize']
        
    def make_plot(self):
        """This method constructs and presents the actual figure."""
        
        # first make a discrete colourscale for the figure. Percentiles are 
        # used for vmin and vmax in an attempt to first filter out any 
        # small-scale extreme artefacts in the data
        vmin = max(np.nanpercentile(self.array, 0.1), 0.)
        vmax = max(np.nanpercentile(self.array, 99.9), 1.)
        levels = MaxNLocator(nbins=8).tick_values(vmin, vmax)
        cmap = plt.get_cmap(COLOURMAP)
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        # define map projection for cartopy plotting
        projection = ccrs.Orthographic(
            central_longitude=RADAR_LON,
            central_latitude=RADAR_LAT
        )
        # then instantiate the figure
        self.fig = plt.figure(figsize=self.size)
        # make the subplots
        for i in range(int(len(self.hours))):
            ax = self.fig.add_subplot(
                *FREQUENCY_DICT[self.freq]['figshape'], i+1,
                projection=projection
            )
            ax.set_extent([-EXTENT, EXTENT, -EXTENT, EXTENT], crs=projection)
            gmap = ax.pcolormesh(self.x, self.y, self.values[i], cmap=cmap, norm=norm)
            ax.coastlines(resolution='10m', linewidth=1.5)
            # plot locations
            for loc in LOCS_TO_PLOT:
                y = LAT_SCALE * (loc[1][0] - RADAR_LAT)
                x = LON_SCALE * (loc[1][1] - RADAR_LON)
                offsets = loc[2]
                ax.plot(x, y, 'ko', markersize=4)
                ax.text(x + offsets[0], y + offsets[1], loc[0], fontsize=14)
            # add subtitle to plot    
            hour = self.hours[i]
            if self.freq != 24:
                plt.title(
                    f'{int2str(hour)}-{int2str(hour + self.freq)} UTC', 
                    fontsize=16
                )
        # add a colorbar
        self.fig.subplots_adjust(right=FREQUENCY_DICT[self.freq]['cbar'][0])
        cbar_ax = self.fig.add_axes(FREQUENCY_DICT[self.freq]['cbar'][1:])
        self.cbar = self.fig.colorbar(gmap, orientation='vertical', cax=cbar_ax)
        self.cbar.ax.tick_params(labelsize=16)
        self.cbar.ax.set_title(self.units, fontsize=16)
