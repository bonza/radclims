"""
prepare.py: module which provides functions for a number of necessary 
preparatory steps on the raw rainfall rate dataarrays, prior to analysis.

Specifically, this module contains the following functions of interest, 
presented in the order of a typical workflow:
    
    1. `append_neighbouring`: this function attaches the last data instance of 
       the previous day, and the first instance of the next day, to a given 
       dataarray containing a day's worth of converted mdv data (as produced in 
       the `mdv.py` module). This is necessary for subsequent interpolation of 
       data from/to the 00Z timestep.
    
    2. `interpolate_to_hour`: inserts rain rates at all on-the-hour timesteps 
       via interpolation between relevant neighbouring data, provided that the 
       time difference between the hour and the closest available data is no 
       greater than 30 minutes.
    
    3. `rates_to_accums`: converts rainfall rates to accumulations via a linear
       interpolation between the rate at the current time step, and that of the 
       most recent previous data, provided the time difference between the two 
       is no greater than 30 minutes.
    
    4. `xhourly_accums`: derives total rainfall accumulations at x-hourly 
       intervals (where x is an integer factor of 24), via summation of the 
       accumulations derived by `rates_to_accums`.
"""


import glob
import pickle
import datetime as dt
import numpy as np
import pandas as pd
import xarray as xr
import os
from radclims.utils import open_pickles, date2subpath, path2date, path2subpath


def append_neighbouring(source_root, dest_root):
    """
    Appends the last (first) timeslots of data from the previous (next) day to 
    all pickled daily xr.DataArray files within `source_root`, if available. 
    This is to allow for subsequent on-the-hour interpolation/insertion of data
    at 00Z timeslots. Writes this extended data as a pickled file to a 
    replicated directory tree within `dest_root`.
    
    Args:
        source_root (str): root directory containing sub-directories of the
        form yyyy/yyyymmdd/yyyymmdd.pick, representing pickled derived rainfall 
        rate data for each day, as produced by the `mdv.py` module). Typically 
        .../data/darwin/rates/raw.        
        dest_root (str): where the replicated directory tree containing the
        extended data is to be located. Typically
        .../data/darwin/rates/extended.        
    """
    paths = glob.glob(f'{source_root}/*/*/*.pick')
    for path in paths:
        data = open_pickles(path)
        date = path2date(path)
        # find available dates immediately before and after current block of
        # data
        neighbouring = []
        for i in [-1, 1]:
            ndate = date + dt.timedelta(days=i)
            npath = f'{source_root}/{date2subpath(ndate)}'
            if os.path.isfile(npath):
                try:
                    ndata = open_pickles(npath)
                    neighbouring.append(ndata[[min(i, 0)]])
                except:
                    pass
        # concatenate neighbouring and original data, and sort by date        
        data = xr.concat(neighbouring + [data], dim='date')
        data = data.reindex(date=sorted(data.date.values))
        
        # write new data to destination as pickled        
        dpath = f'{dest_root}/{date2subpath(date, with_file=False)}'
        if not os.path.isdir(dpath):
            os.makedirs(dpath)
        with open(f'{dest_root}/{date2subpath(date)}', 'wb') as new:
                pickle.dump(obj=data, file=new)


def generic_func_wrapper(source_root, dest_root, func):
    """
    Applies `func` to each file in the directory tree starting at
    `source_root`, pickles the result and writes this to a replica path in
    `dest_root`. Intended for use with `interpolate_to_hour`, 
    `rates_to_accums`, and `xhourly_accums` to avoid repeated code.
    
    Args:
        source_root (str): the root directory containing sub-directories of the
        form /yyyy/yyyymmdd/yyyymmdd.pick, upon which `func` is to be applied.
        dest_root (str): the root directory within which the replicated 
        directory tree containing the modified data files is to be written.
        func (function): the function to be applied.
    """
    paths = glob.glob(f'{source_root}/*/*/*.pick')
    # iterate through all pickled files, running `func` on each
    for path in paths:
        data = open_pickles(path)
        try:  
            # run func over the daily datasets
            result = func(data)
            # specify destination path for the result
            dest_path = f'{dest_root}/{path2subpath(path)}'
            # if path doesn't exist, make it            
            if not os.path.isdir(dest_path):
                os.makedirs(dest_path)
            # pickle the result in the destination path    
            with open(f'{dest_path}/{dest_path[-8:]}.pick', 'wb') as new:
                    pickle.dump(obj=result, file=new)
        # if this doesn't work move onto next path
        except:
            pass


def interpolate_to_hour(data):
    """
    Conditionally inserts data at all on-the-hour timeslots via linear 
    interpolation across the hour, so long as the available across-hour 
    interval of is less than half an hour. 
    
    In a typical workflow, this will be fed into `generic_func_wrapper`.
    
    Args:
        data (xarray.DataArray): the dataarray to which interpolation is to be
        applied.
        
    Returns:
        new_data (xarray.DataArray): a new dataarray containing the data from
        `data`, plus the new interpolated on-the-hour data points.
    """
    dates_array = data.date.values
    dates_array_shifted = np.roll(dates_array, -1, axis=0)
    dates_array_shifted[-1] = np.datetime64('NaT')
    # make a datetimeindex out of the original xarray dates
    dates = pd.DatetimeIndex(dates_array)
    # get date intervals in seconds
    time_diff = (pd.DatetimeIndex(dates_array_shifted) - dates).total_seconds()
    # get minute data, convert to series as easiest way to shift
    mins = pd.Series(dates.minute)
    # subtract minute data from shifted minute data. Where result
    # is negative, can assume it crosses the hour (unless time in
    # minutes in the next hour is greater than for the current, but
    # in that case wouldn't want to include anyway as the gap is over
    # an hour so is too big i.e. inaccurate interpolation. 
    mins_shifted = mins.shift(periods=-1)
    mins_diff = (mins_shifted - mins).values
    
    # find the index position of before-hour dates for all instances 
    # where difference is negative, interval to next time is less than half hour,
    # and not already on the hour (minute=0)
    pre_hour_ind = np.where(
        (mins_diff < 0) & (time_diff < 1800.) & (mins_shifted.values != 0))[0]
    
    # pull out the actual dates (both before and after-hour where we
    # have a cross-over)
    pre_hour_dates = dates[pre_hour_ind]
    post_hour_dates = dates[pre_hour_ind + 1]
    # convert dates to an on-the-hour base
    new_dates = post_hour_dates.floor('H')
    
    # contract original data to the valid crossover dates (one
    # for before, one for after the hour)
    post_hour_data = data.sel(date=post_hour_dates) 
    pre_hour_data = data.sel(date=pre_hour_dates)
    # time in hours of the interval from pre-hour date to post-
    # hour date
    interval = ((
        pd.DatetimeIndex(post_hour_data.date.values) - (
                pd.DatetimeIndex(pre_hour_data.date.values))
    ).total_seconds() / 3600.).values
    
    # broadcast this onto the xarray space dimensions (
    # preserving date dim of course)
    interval_reshape = np.broadcast_to(
        np.reshape(interval, (len(interval), 1, 1)),
        post_hour_data.shape)
    
    # time in hours from before-hour date to the hour
    # crossover
    interval_to_hour = ((
        new_dates - pd.DatetimeIndex(pre_hour_data.date.values)
    ).total_seconds() / 3600.).values
    # broadcast this onto the xarray space dims
    interval_to_hour_reshape = np.broadcast_to(
        np.reshape(interval_to_hour, (len(interval_to_hour), 1, 1)),
        post_hour_data.shape)
    
    # gradient across the hour
    m = (post_hour_data.values - pre_hour_data.values) / interval_reshape
    # time from start point to the hour
    t = interval_to_hour_reshape
    # start point for interpolation (data at before-hour date)
    c = pre_hour_data.values
    # linear interpolation; result
    y = m * t + c
    
    # make an xarray from the interpolated data with the
    # on-the-hour dates as the date dim
    new_data = xr.DataArray(
        data = y,
        coords={'date': new_dates, 'x': data.x, 'y': data.y},
        dims=['date', 'x', 'y'])
    
    # join the original and interpolated xarrays
    new_data = xr.concat([data, new_data], dim='date')
    # reorder the date dimension
    new_data = new_data.reindex(date=np.array(sorted(new_data.date.values)))
    # inserted
    
    return new_data


def rates_to_accums(data):
    """
    Convert instantaneous rain rates to rainfall accumulations, based on linear 
    interpolation, where accumulation from time a to b is calculated as the 
    rain rate at time a multiplied by the length of the interval from a to b.
    If the interval exceeds 30 minutes (1800 seconds), accumulation is 
    recorded as a NaN. Returns dataarray in units mm.
    
    In a typical workflow, this will be fed into `generic_func_wrapper`.
    
    Args:
        data (xarray.DataArray): dataarray containing rain rates (typically has
        already undergone on-the-hour interpolation by `interpolate_to_hour`)
            
    Returns:
        accums (xarray.DataArray): dataarray containing the derived
        accumulations (in units mm).
    """
    dates = pd.DatetimeIndex(data.date.values)
    dates_shift = pd.DatetimeIndex(
        pd.Series(dates).shift(periods=-1))
    # get length in seconds of each interval in dates dimension
    intervals = ((dates_shift - dates).total_seconds()).values
    # all intervals greater than 30 min get nanned
    intervals[intervals > 1800] = np.nan
    # broadcast to xarray shape
    intervals_broadcast = np.broadcast_to(
        np.reshape(intervals, (len(intervals), 1, 1)),
        data.shape)
    # multiply rain rates xarray (mm/hr) by time of interval to 
    # get accumulated rain over that interval
    accums = intervals_broadcast * data / 3600.
    
    return accums


def filter_by_interval(data, interval, by='date'):
    """
    Filters `data` according to `interval` (a tuple). If `by` == 'date', 
    function expects `interval` to contain dates (e.g. strings). If
    `by` == 'hour', function expects `interval` to contain integers. This
    function is only used by `xhourly_accums`.
    
    Args:
        data (xarray.DataArray): the data to be filtered.
        interval (tuple): contains either date strings 'yyyymmdd' or hour 
        integers, depending on whether `by` == 'date' or 'hour'. 
        by (str, default 'date'): specifies whether to filter by a date 
        interval, or an hour interval.
        
    Returns:
        filtered (xarray.DataArray): the filtered data.
    """
    # interval is a tuple - unpack
    start, end = interval
    dates = pd.DatetimeIndex(data.date.values)
    
    # create mask according to interval
    if by == 'date':
        mask = (dates >= start) & (dates < end)
    elif by == 'hour':
        mask = (dates.hour >= start) & (dates.hour < end)
    else:
        raise ValueError("Bad `by`.")
    
    # filter data according to mask
    filtered = data.sel(date=dates[mask])
    
    return filtered


def xhourly_accums(data, x=1):
    """
    Aggregates accumulation data over on-the-hour intervals of `x` hours. `x` 
    must be an integer factor of 24. Output is a dataarray containing 
    accumulated rainfall at on-the-hour timeslots (e.g. hour 0, 3, 6, etc. if 
    x=3). Preserves unique dates.
    
    Args:
        data (xarray.DataArray): accumulation data, to be aggregated to
        on-the-hour accumulations. Must already contain on-the-hour 
        accumulation entries (amongst others).
        x (int): the desired interval length, in hours, between adjacent data. 
        Must be an integer factor of 24.
        
    Returns: 
        xhourly_data (xarray.DataArray): contains total rainfall accumulations 
        at x-hourly intervals, preserving unique dates.
            
    Raises: 
        ValueError if `x` is not an integer factor of 24.
    """
    # make sure 24 is a multiple of x
    if 24 % x != 0:
        raise ValueError("Bad `x`, needs to be a factor of 24.")
    
    # DatetimeIndex of input data
    dates = pd.DatetimeIndex(data.date.values)
    # date indices where hour is multiple of x and minute = 0 
    # (date is on the hour)
    hours_inds = np.where(
        (dates.hour % x == 0) & (dates.minute == 0))[0]
    # feed date indices back into dates to get
    # only on-the-hour x-hourly dates
    hours = dates[hours_inds]
    hours_shift = pd.DatetimeIndex(
        pd.Series(hours).shift(periods=-1))
    # delete obsolete variables
    del dates, hours_inds
    
    # flag for first run through the processing loop
    firstrun = True
    for date1, date2 in zip(hours, hours_shift):
        # check if date is x hours ahead. If not, data is considered
        # incomplete for that interval, and ommitted
        if date2 == (date1 + pd.DateOffset(hours=x)):
            current_date = np.array([date1])
            sub_data = filter_by_interval(data, (date1, date2)).values
            agg = np.sum(sub_data, axis=0, keepdims=True)
            # if first run through, assign variables
            if firstrun:
                new_dates = current_date
                new_data = agg
                firstrun = False
            # else concatenate to previous variables (along date dim)
            else:
                new_dates = np.concatenate(
                    [new_dates, current_date],
                    axis=0)
                new_data = np.concatenate(
                    [new_data, agg],
                    axis=0)
    # dates into DatetimeIndex for DataArray
    new_dates = pd.DatetimeIndex(new_dates)
    # generate DataArray
    xhourly_data = xr.DataArray(
        data=new_data,
        coords={'date': new_dates, 'x': data.x, 'y': data.y},
        dims=['date', 'x', 'y'])
    
    return xhourly_data                
