"""
analyse.py: offers two types of statistical analysis of regime/month-based 
x-hourly rainfall accumulation dataarrays, namely: probability of >Xmm of 
precipitation (PoPX) at a given point; and the Yth percentile of precipitation 
at a given point (PrecipY).

Unlike the previous modules (`mdv`, `prepare`, and `stratify`), this module 
does not directly write anything to file. Given the potentially large number of 
parameter permutations possible at this point (could be 1000s), it can be 
be unwieldy to have to save a separate dataarray for each permutation to file. 
Therefore, this module is instead used directly by the `data_composer` in the 
`compose` module, which then creates the corresponding figure on the fly, and
saves that figure to file instead, with a systematically constructed filename.
"""


import numpy as np
import pandas as pd
import xarray as xr


def PoPX(data, thresh=0.2, conditional=False):
    """
    Takes an x-hourly xarray.DataArray, discretises each xhourly accumulation
    according to `thresh`, then aggregates each hour's data across all dates.
    Returns spatial probabilities of rain > `thresh` for each hour interval in 
    `data`, in a xarray.DataArray. If `conditional` is True returns conditional
    probabilities of rain > `thresh` (given any rain has occurred).
    
    Args:
        data (xarray.DataArray): x-hourly rainfall accumulation data, 
            containing unique date coordinates for each data point. Typically 
            this data would be found in .../data/darwin/accumulations/
            concatenated/<frequency>/<regime no.>.
        thresh (float): corresponds to the 'X' in 'PoPX' - the amount of
            precipitation in mm for which the probability of exceedence is
            calculated.
        conditional (bool): option for conditional probability - specifies 
            whether probability of exceedence is based on all data, or only
            instances where >0 mm of rain has occurred. Useful for 
            investigating rain intensity probabilities, as opposed to 
            occurrence probs. 'False' corresponds to the all-days scenario.
        
    Returns:
        result (xarray.DataArray): spatial probabilities of exceeding X mm 
            within the x-hourly interval beginning at the given 'hour' 
            coordinate. The datarray has spatial dimensions 'x' and 'y', and 
            time dimension 'hour'. The coordinates of 'hour' correspond to the 
            unique x-hourly intervals contained in the range [00Z, 24Z) (e.g.
            if x is 3, these intervals will be [00Z, 03Z, 06Z, 09Z, 12Z, 15Z, 
            18Z, 21Z]).
    """
    dates = pd.DatetimeIndex(data.date.values)
    hours = set(dates.hour)
    # store spatial coordinates and dates 
    x = data.x
    y = data.y 
    # discretise each timestep in data according to whether `thresh` was
    # exceeded (0 or 1)
    disc_by_thresh = (data > thresh).astype(int)
    # if want conditional probs, need to discretise by 'any' rain also
    if conditional:
        disc_by_any_rain = (data > 0.).astype(int) 
    # loop through hours, aggregate data for each hour, put into
    # new xarray.DataArray with time dim 'hour' (single value)
    arrays = []
    for hour in hours:
        inds = np.where(dates.hour == hour)[0]
        sub = disc_by_thresh[inds]
        mean_sub = sub.mean(dim='date', skipna=True).values
        # if conditional then cond prob is calculated against chance of any rain
        if conditional:
            sub_any = disc_by_any_rain[inds]
            mean_sub_any = sub_any.mean(dim='date', skipna=True).values
            # calculate conditional prob
            mean_sub = np.nan_to_num(mean_sub / mean_sub_any)
        # pack it into a dataarray and append to `arrays`
        mean_sub = xr.DataArray(
            data=[mean_sub],
            dims=['hour', 'x', 'y'],
            coords={'hour': [hour], 'x': x, 'y': y})
        arrays.append(mean_sub)
    # build new xarray.DataArray    
    result = 100 * xr.concat(arrays, dim='hour')
    return result


def grid_percentiles(
        data, q, dims, conditional=False, keep_hours=False, squeeze=False):
    """
    Takes an accumulation dataarray `data`. Determines the percentiles given by 
    `q`, collapsed across the dimensions given by `dims`. Preserves unique
    hour coordinates from `data` via a new 'hour' dimension, if `keep_hours` is 
    'True'. This function represents the general form of `PrecipY`.
    
    Args:
        data (xarray.DataArray): x-hourly rainfall accumulation data, 
            containing unique date coordinates for each data point. Typically 
            this data would be found in .../data/darwin/accumulations/
            concatenated/<frequency>/<regime no.>.
        q (list of floats in range [0, 100]): percentiles to be calculated.
        dims (list of str): dimensions of `data` that are to be collapsed.
        conditional (bool): option for conditional percentiles - specifies 
            whether the rainfall percentile is based on all data, or only on
            instances where >0 mm of rain has occurred. 'False' corresponds to 
            the all-days scenario.
        keep_hours (bool): introduce an 'hour' dimension to the resulting data,
            where each unique hour in the original 'date' coords of `data` 
            represents a coordinate of 'hour'.
        squeeze (bool): squeeze out the 'percentile' dimension of `result`. 
            This should only be used if `q` contains a single value.
            
    Returns:
        result (xarray.DataArray): spatial rainfall percentiles for each 
            percentile contained within `q`. The dimensions of the resulting 
            dataarray are determined by:
                - `q`: a new 'percentile' dimension is introduced, with 
                  coordinates being the values in `q`
                - `dims`: the dimensions of `data` complement to `dims` are 
                  preserved in the result,
                - `keep_hours`: if 'True', a new dimension 'hour' is 
                  introduced, containing the unique hours (ints) of `data` as 
                  its coordinates,
                - `squeeze`: if 'True' and `q` contains only a single value,
                  the 'percentile' dimension is subsequently squeezed before
                  returning `result`.
                  
    Raises:
        ValueError if len(q) > 1 and squeeze == True.
    """
    if len(q) > 1 & squeeze:
        raise ValueError(
            "Cannot squeeze 'percentile' dimension when len(q) > 1!"
        )
    
    def _grid_percentiles(data, q, dims, conditional):
        """
        Determines rainfall percentiles of `data` without constructing the new
        'hour' dimension. Private function of `grid_percentiles`, same args.
        """
        # get values in array format. Convert zeros to nans if result is 
        # conditional on any rain
        array = data.values
        if conditional:
            array[array <= 0.] = np.nan
        # determine axes in `array` over which to calculate percentiles
        data_dims = list(data.dims)
        axis = [data_dims.index(dim) for dim in dims]
        keep_dims = [dim for dim in data_dims if dim not in dims]
        # calculate percentiles across axis, ignoring nans
        result = np.nanpercentile(array, q, axis=axis)
        # dims and coords of resulting dataarray
        new_dims = ['percentile', *keep_dims]
        new_coords = dict(
            zip(new_dims, [q]+[data.coords[i].values for i in keep_dims])
        )
        result = xr.DataArray(data=result, dims=new_dims, coords=new_coords)        
        return result
    
    # if preserving hours then the process is slightly more complicated
    if keep_hours:
        # first group `data` by hour into a groupby object
        grouped = data.groupby('date.hour')
        results = []
        # iterate through the groupby object and calculate percentiles on each        
        for hour, group in grouped:
            result = _grid_percentiles(group, q, dims, conditional)
            result_dims = result.dims
            # create new set of dims and coords which contain hour
            new_dims = ['hour', *list(result_dims)]
            new_coords = dict(
                zip(new_dims, 
                    [[hour]]+[result.coords[i].values for i in result_dims])
            )
            # pack it into a datarray with the new hour dimension
            result = xr.DataArray(
                data=np.expand_dims(result.values, 0),
                dims=new_dims,
                coords=new_coords
            )
            # put this into a list for easy concatenation
            results.append(result)
        # concatenate along hour dimension, and transpose result to put 
        # 'percentile' dim at axis 0 (purely cosmetic)
        result = xr.concat(results, dim='hour')
        result = result.transpose('percentile', 'hour', 'x', 'y') 
        if squeeze:
            result = result.squeeze(dim='percentile', drop=True)                   
        return result
    
    # otherwise calculate without hours preserved
    result = _grid_percentiles(data, q, dims, conditional)
    return result


def PrecipY(data, percentile, conditional):
    """
    Specific instance of `grid_percentiles`. Calculates for only a single 
    percentile value. Collapses the 'date' dimension of `data`, but introduces
    an 'hour' dimension containing unique hour coordinates (corresponding to
    the x-hourly spacing of `data`). Automatically queezes out the 
    single-coordinate 'percentile' dimension.
    
    Args:
        data (xarray.DataArray): x-hourly rainfall accumulation data, 
            containing unique date coordinates for each data point. Typically 
            this data would be found in .../data/darwin/accumulations/
            concatenated/<frequency>/<regime no.>.
        percentile (float): the single percentile to be calculated.
        conditional (bool): option for conditional percentiles - specifies 
            whether the rainfall percentile is based on all data, or only on
            instances where >0 mm of rain has occurred. 'False' corresponds to 
            the all-days scenario.
            
    Returns:
        result (xarray.DataArray): spatial rainfall percentiles, according to 
            the specified `percentile`, within the x-hourly interval beginning 
            at the given 'hour' coordinate. The datarray has spatial dimensions 
            'x' and 'y', and time dimension 'hour'. The coordinates of 'hour' 
            correspond to the unique x-hourly intervals contained in the range 
            [00Z, 24Z) (e.g. if x is 3, these intervals will be [00Z, 03Z, 06Z, 
            09Z, 12Z, 15Z, 18Z, 21Z]).
    """
    result = grid_percentiles(
        data, [percentile], dims=['date'], conditional=conditional, 
        keep_hours=True, squeeze=True
    ) 
    return result
