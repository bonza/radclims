"""
mdv.py: module for reading raw radar MDV (dbz) files into Python, and 
conversion of radar reflectivities into rainfall rates (mm/hr).

This module serves two primary purposes:
    
    1. To read raw MDV files into Python, with a subsequent conversion of the
       raw reflectivities into rainfall rates (mm/hr);
    2. To then write these converted MDVs back to file as pickled 
       xarray.DataArray files, in a directory tree mirroring that of the 
       original MDVs.
       
The function `mdvs_to_dataarray` carries out the above tasks. 
"""

import pyart
import numpy as np
import xarray as xr
import pandas as pd
import os
import datetime as dt
import glob
import pickle


def dbz_to_mmhr(data):
    """
    Convert a dBz array to approximate rain rate (mm/hr) via the
    Marshall-Palmer formula.
    
    Args:
        data (array-like): reflectivity (dbz) data to be converted.
        
    Returns:
        mm_per_hr (array-like): preserves data-type from `data`, contains the
        derived rainfall rate data in units mm/hr.
    """
    # Marshall-Palmer formula
    mm_per_hr = (10.**(data/10.)/200.)**(5./8.)    
    return mm_per_hr


def read_mdv(path, convert=True, zindex=1):
    """
    Reads an mdv file to numpy array, converts dbz to rates (if specified), and 
    slices data at a specified vertical layer (default corresponds to 1000 m, 
    though this may not hold accross different radars). Also return datetime.
    
    Args:
        path (str): path to mdv file to be read.
        convert (bool): specifies whether reflectivities should be converted
        to rainfall rates or not.
        zindex (int): the index of the z-level for which we want data. It is
        likely that the default value should be changed for different radars
        (for Gunn Pt, the index 1 corresponds to 1000m altitude).
            
    Returns:
        (date, data): date is an numpy.datetime64 object representing the date
        and time of the mdv file being read; data is a two dimensional numpy
        array.
    """
    # path needs to be absolute; get date from path
    datestring = f'{path[-19:-4]}'.replace('/', '')
    date = dt.datetime.strptime(datestring, '%Y%m%d%H%M%S')
    # convert to datetime64 object
    date = np.datetime64(date)    
    # read mdv, get reflectivity data corresponding to index level
    data = pyart.io.mdv_grid.read_grid_mdv(path)
    data = data.fields['reflectivity']['data'][zindex].data    
    # convert data to rain rate if desired
    if convert:
        data = dbz_to_mmhr(data)    
    # return tuple    
    return date, data


def read_mdv_dir(path):
    """
    Reads a day directory of mdv files, slices this data at a single vertical 
    height level, converts the data to rainfall rates, and puts into (returns) 
    a DataArray with the dimensions 'date' (time), 'x', 'y'. Returns a 
    ValueError if the specified path is empty.
    
    Args:
        path (str): path containing a single days' worth of individual mdv 
        files.
        
    Returns:
        data (xarray.DataArray): contains data from all mdv files within 
        `path` (a days' worth), concatenated into a single dataarray with 
        spatial dimensions 'x' and 'y', and time dimension 'date'.
        
    Raises:
        ValueError if no mdv files (with the suffix '.mdv') are contained
        within `path`.
    """    
    # set up data containers. 200 date coords will be
    # sufficient for one day of data
    dates = []
    shape = (200, 121, 121)
    data = np.zeros(shape)
    # list of mdvs within path
    mdvs = glob.glob(f'{path}/*.mdv')
    if len(mdvs) > 0:
        # loop through mdvs, read each
        for mdv in mdvs:
            new_date, new_data = read_mdv(mdv)
            data[len(dates)] = new_data
            dates.append(new_date)            
        # trim the data according to number of dates
        data = data[:len(dates)]
        dates = pd.DatetimeIndex(dates)    
        # whack it all in a DataArray
        data = xr.DataArray(
            data=data,
            coords={'date': dates},
            dims=['date', 'x', 'y']
        )
        # sort dates (all jumbled up in directory)
        data = data.reindex(date=sorted(data.date.values))       
        return data
    else:
        raise ValueError(f'{path} is an empty directory.')
        
        
def mdvs_to_dataarray(source_root, dest_root):
    """
    Reads each day of mdv files (multiple) below source directory into a single 
    DataArray, converts reflectivity data to rainfall rates, then pickles the 
    data into a duplicate directory tree.
    
    Args:
        source_root (str): location of mdv files. Function expects this root 
        directory to contain ONLY sub-directories of the form 
        yyyymmdd/hhmmss.mdv (each file represents a single complete radar 
        scan). Typically .../data/darwin/mdvs.
        
        dest_root (str): the location to where the converted and concatenated 
        mdv files are to be written (with each days' worth of mdvs as a single, 
        pickled xarray.DataArray file). A directory tree identical to that 
        within `source_root` is created in `dest_root`, and the pickled files 
        are written into this tree in the form yyyymmdd.pick. Typically
        .../data/darwin/rates/raw.
    """
    # get list of source_root/yyyy/yyyymmdd directories
    dirs = glob.glob(f'{source_root}/*/*')
    for d in dirs:
        path = f'{dest_root}/{d[-13:]}'
        # replicate directory tree within dest_root: if d doesn't 
        # already exist (e.g. from previous run), then make it
        if not os.path.isdir(path):
            os.makedirs(path)
        # read all mdvs (day's worth) in d (if they exist), pickle the result
        try:
            da = read_mdv_dir(d)
            with open(f'{path}/{path[-8:]}.pick', 'wb') as f:
                pickle.dump(obj=da, file=f)
        # if it doesn't work, delete the empty directory
        except:
            os.rmdir(path)