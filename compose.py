"""
compose.py: contains the function `data_composer`, the bridge between the 
stored accumulations data and the final results, ready to be plotted using the 
`plot.py` module.

`data_composer` acts as the interface between the prepocessed accumulation data 
stored in .../data/darwin/accumulations/concatenated/ (previously produced via 
the `prepare` and `stratify` modules), and the `SpatialPlot` class within 
`plot.py`. It calls on the analysis functions contained within `analyse.py` in 
order to derive the data to be plotted, and then does some other minor 
modifications of the data to get it to a form ready to be delivered to the 
`SpatialPlot` object. Therefore, this function represents the penultimate step 
in a typical workflow.
"""


import warnings
import numpy as np
import xarray as xr
from radclims.config import (
    ACCUMS_PATH, DEFAULT_GRIDSIZE, PARAMETER_DICT, RADAR_RANGE, RADAR_LOC
)
from radclims.utils import open_pickles

# `data_composer` raises a DeprecationWarning for interpreting floats as 
# integers - this is nothing to worry about
warnings.filterwarnings('ignore', category=DeprecationWarning)

def data_composer(regime, freq, param, conditional):
    """
    Extracts, processes, and composes data from the accumulated rainfall data, 
    according to the specified args, into a form suitable for plotting. The 
    args provided to this function are used to access preset configurations 
    within `config.py` (dictionaries).
    
    Args:
        regime (int): the regime/month number for which data is to be 
            processed.
        freq (int): the x-hourly frequency of the data, typically 3 or 24 are 
            the options.
        param (str): 'PoPX' ('PrecipY'), where 'X' ('Y') is replaced with the
            desired rainfall threshold (percentile).
        conditional (bool): option for the results to be conditional on either 
            all data ('False'), or only instances where >0 mm has occurred
            ('True').
            
    Returns:
        data (xarray.DataArray): the processed data, in a form suitable for 
            plotting using the `plot.py` module. `data` has the spatial 
            dimensions 'x' and 'y', which have coords as distance from radar
            in km in each direction, and the 'hour' dimension, whose coords
            depend on `freq`.
    """
    path = ACCUMS_PATH.format(freq, regime)
    data = open_pickles(path)
    data = PARAMETER_DICT[param]['func'](data, conditional=conditional)
    hours = np.linspace(0, 24 - freq, 24 / freq).astype(int)
    res = np.shape(data)[-1]
    sp_coords = np.linspace(0, res - 1, res).astype(int)
    # rescale spatial coords as dist km from radar
    sp_coords = DEFAULT_GRIDSIZE * (sp_coords - (len(sp_coords) - 1) / 2.)
    data = xr.DataArray(
        data=data,
        dims=['hour', 'x', 'y'],
        coords={'hour': hours, 'x': sp_coords, 'y': sp_coords}
    )
    data.attrs = {
        'regime': regime,
        'parameter': param, 
        'units': PARAMETER_DICT[param]['units'],
        'frequency': freq, 
        'grid_spacing': DEFAULT_GRIDSIZE}
    
    # nan out all data beyond radar range
    data = data.where(np.sqrt(data.x**2 + data.y**2) < RADAR_RANGE)
    # set the radar grid (centre) to np.nan
    data[:, RADAR_LOC[0], RADAR_LOC[1]] = np.nan        
    return data
