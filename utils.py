"""
utils.py: a collection of simple functions repeatedly used during the data 
processing and analysis steps.
"""


import glob
import pickle
import numpy as np
import datetime as dt


def open_pickles(path):
    """
    Convenience function to open pickled data.
    
    Args:
        path (str): path to the pickle file to be opened.
    """
    with open(path, 'rb') as f:
        data = pickle.load(f)
    return data


def int2str(i):
    """
    Takes an integer corresponding to a day or month and converts
    it to a length two string e.g. 1 -> '01', 10 -> '10'.
    
    Args:
        i (int): integer to be converted to string format.
        
    Returns:
        string (str): minimum length-2 string representation of `i`.
    """
    string = str(i)
    if len(string) == 1:
        string = f'0{string}'        
    return string


def date2subpath(date, with_file=True, suffix='.pick'):
    """
    Takes a datetime.date object and generates the sub-path string
    corresponding to the pickled file for that date, e.g.
    datetime.date(2016, 1, 1) -> 2016/20160101/20160101.pick.
    
    Args:
        date (datetime.date): the date used in the returned sub-path string.
        with_file (bool): specifies whether or not the actual filename is
        included in the output.
        suffix (str): the desired filename extension.
            
    Returns:
        path (str): date-indexed path to file, of the form 
        yyyy/yyyymmdd/yyyymmdd.suffix.
    """
    year = str(date.year) 
    month = int2str(date.month)
    day = int2str(date.day)
    if with_file:
        path = f'{year}/{year}{month}{day}/{year}{month}{day}{suffix}'
    else:
        path = f'{year}/{year}{month}{day}'
    return path


def path2date(path, suffix='.pick'):
    """
    Takes a path to a pickled file with file naming format `yyyymmdd`,
    returns a datetime.date object.
    
    Args:
        path (str): the path string from which the date is extracted.
        suffix (str): default '.pick'. The filename extension, the length of
        this extension is used for parsing.
        
    Returns:
        date (datetime.date): date object derived from the path string.
    """
    slength = len(suffix)
    datestring = path[(-1 * slength - 8):(-1 * slength)]
    year = int(datestring[:4])
    month = int(datestring[4:6])
    day = int(datestring[6:8])
    date = dt.date(year, month, day)
    return date
    
    
def path2subpath(path, suffix='.pick'):
    """
    Takes an absolute path to a file, assuming format 
    .../yyyy/yyyymmdd/yyyymmdd.suffix, and returns the corresponding subpath of
    the form yyyy/yyyymmdd.
    
    Args:
        path (str): path to file for which subpath is to be extracted.
        suffix (str): filename extension of the file.
            
    Returns:
        subpath (str): the sub-path to the file, two directories deep - e.g.
        returns yyyy/yyyymmdd from .../yyyy/yyyymmdd/yyyymmdd.pickle.
    """
    slength = len(suffix)
    datestring = path[(-1 * slength - 8):(-1 * slength)]
    year = datestring[:4]
    subpath = f'{year}/{datestring}'
    return subpath
    

def paths2datelist(source_root):
    """
    Constructs a list of dates, with each date in the form 'yyyy-mm-dd', from a 
    specified root directory which contains only sub-paths of the form 
    root/yyyy/yyyymmdd/...
    
    Args:
        source_root (str): root directory containing sub-paths of the form 
        yyyy/yyyymmdd/...
        
    Returns:
        datelist (list of str): a list containing all unique dates, determined 
        by the sub-paths within `source_root`, and with each date reconstructed
        into the form 'yyyy-mm-dd'.
    """
    paths = glob.glob(f'{source_root}/*/*')
    datelist = [f'{path[-8:-4]}-{path[-4:-2]}-{path[-2:]}' for path in paths]    
    return datelist


def smooth_dataarray(dataarray, r, disc=True, thresh=0.2):
    """
    Applies smoothing to a xarray.DataArray over the spatial dimensions via
    spatial averaging with a square filter. Preserves any nans present in the
    original data.
    
    Args:
        dataarray (xarray.DataArray): the data array to be smoothed.
        r (int): the number of neighbouring grid cells in the x and y 
            directions to be included in the smoothing operation at a given 
            point.
        disc (bool): whether the final result will be discretised to ones and
            zeros based on a specified threshold.
        thresh (float): if `disc` is True, the threshold used to discretise
            the result.
            
    Returns:
        result (xarray.DataArray): a new spatially smoothed data array. 
            Preserves dimensions and coordinates, and any nans present in the 
            original data.
    """
    # pull out data array values as numpy array
    data = dataarray.values
    shape = data.shape
    # if data is only two-dimensional, add a third outer dim
    if len(shape) == 2:
        data = np.array([data])
        shape = data.shape
    # create a container of shape `shape`, but with added inner dimension with 
    # length equal to the number of neighbouring points used by the smoothing
    # filter (this is given by ((2 * r + 1) ** 2)
    new_data = np.zeros((*shape, (2 * r + 1) ** 2))
    # iterate through the dimensions, and collect neighbouring points for each
    # spatial point
    for i in range(shape[0]):
        for j in range(r, shape[1]-r):
            for k in range(r, shape[2]-r):
                if not np.isnan(data[i,j,k]):
                    nbours = data[i,j-r:j+r+1,k-r:k+r+1].flatten()
                    new_data[i,j,k,:] = nbours
    # calculate the mean across the added fourth dimension
    new_data = np.nanmean(new_data, axis=3)
    # get rid of third dimension if not present in original data
    new_data = np.squeeze(new_data)
    # convert means to ones and zeros if specified
    if disc:
        new_data = (new_data >= thresh).astype(int)
    # preserve any nans present in the original data
    mask = ~np.isnan(data)
    new_data = np.where(mask, new_data, np.nan)
    # pack new data into original data array
    result = dataarray.copy()
    result.values = new_data
    return result
