"""
make_figures.py: script file to construct the climatology figures.

Loops through all possible combinations of parameters, as specified within 
`config.py`, creates the corresponding figure for each parameter combination,
and saves the figure as a .png file to a specified directory 
(`config.FIGURES_PATH`). 
"""


import warnings
from radclims.plot import SpatialPlot
from radclims.compose import data_composer
from radclims.config import (
    FIGURES_PATH, FREQUENCY_DICT, REGIMES_DICT, PARAMETER_DICT, CONDITIONALS
)

# a few unimportant warnings are raised when making the figures, easiest 
# (though not best practise...) to just disable all warnings
warnings.filterwarnings('ignore')

# iterate through each of the parameters available to `data_composer` in order
# to create a figure for every possible combination; save figure to file
for reg in REGIMES_DICT:
    for freq in FREQUENCY_DICT:
        for param in PARAMETER_DICT:
            for cond in CONDITIONALS:
                # create dataarray based on current parameters
                data = data_composer(REGIMES_DICT[reg], freq, param, cond)
                # instantiate `SpatialPlot` with current dataarray
                figure = SpatialPlot(data)
                # create the figure and save to file
                figure.make_plot()
                filename = (
                    f"{reg}_{FREQUENCY_DICT[freq]['name']}_{param}_{cond}.png"
                )
                figure.fig.savefig(
                    f"{FIGURES_PATH}/{filename}",
                    bbox_inches = 'tight',
                    pad_inches = 0 
                )