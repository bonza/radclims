"""
config.py: various default configurations for the `radclims` package.
"""

import functools
import numpy as np
from radclims.analyse import PoPX, PrecipY


# a bunch of directories, paths, pathname formats that will be used often
ROOT = '/Volumes/ELEMENTS/radar_climatologies'
REGIMES_PATH = f'{ROOT}/data/darwin/regimes.pickle'
MONTHS_PATH = f'{ROOT}/data/darwin/months.pickle'
ACCUMS_PATH = (
    f'{ROOT}/data/darwin/accumulations/concatenated/{{}}h/{{}}/data.pick'
)
FIGURES_PATH = f'{ROOT}/data/darwin/figures'

# colourmap for the figures
COLOURMAP = 'coolwarm'

# geographic radar parameters
RADAR_LAT = -12.25
RADAR_LON = 131.04
EXTENT = 130000
# Lat to m scaling factor (m/deg) (constant)
LAT_SCALE = 1000 * 110.574
# Lon to m scaling factor (m/deg) (latitude-specific)
LON_SCALE = 1000 * 111.320 * np.cos(np.radians(RADAR_LAT))
# Radar range in km
RADAR_RANGE = 125
# Radar data grid size in km
DEFAULT_GRIDSIZE = 2.5
# xy radar location with respect to grid indices
RADAR_LOC = (60, 60)

# mapping of regime/month names to their associated ids, as represented in the
# darwin/data... directory
REGIMES_DICT = {
    'Wet-season-all': 0,
    'Early-build-up': 1, 
    'Late-build-up': 3,
    'Build-up-all': 6,
    'Active-monsoon': 2,
    'Westerly-monsoon-break': 4,
    'Easterly-monsoon-break': 5,
    'Monsoon-break-all': 7,
    'October': 8,
    'November': 9,
    'December': 10,
    'January': 11,
    'February': 12,
    'March': 13,
    'April': 14,
}

# used by `run_stratification` (`stratify` module) - mapping of regime/month 
# ids to actual regime/month numbers
STRAT_DICT = {
    0: ([1, 2, 3, 4, 5], REGIMES_PATH),  # wet season all
    1: ([1], REGIMES_PATH),  # early build-up
    2: ([2], REGIMES_PATH),  # active monsoon
    3: ([3], REGIMES_PATH),  # late build-up
    4: ([4], REGIMES_PATH),  # westerly monsoon break
    5: ([5], REGIMES_PATH),  # easterly monsoon break
    6: ([1, 3], REGIMES_PATH),  # build-up all
    7: ([4, 5], REGIMES_PATH),  # monsoon break all
    8: ([10], MONTHS_PATH),  # october
    9: ([11], MONTHS_PATH),  # november
    10: ([12], MONTHS_PATH),  # december
    11: ([1], MONTHS_PATH),  # january
    12: ([2], MONTHS_PATH),  # february
    13: ([3], MONTHS_PATH),  # march
    14: ([4], MONTHS_PATH)  # april
}

# used by `data_composer` function (`compose` module) - a mapping of parameters
# to their associated data units and function specs
PARAMETER_DICT = {
    'PoP': {'units': '%', 'func': functools.partial(PoPX, thresh=0.2)},
    'PoP1': {'units': '%', 'func': functools.partial(PoPX, thresh=1.)},
    'PoP5': {'units': '%', 'func': functools.partial(PoPX, thresh=5.)},
    'PoP15': {'units': '%', 'func': functools.partial(PoPX, thresh=15.)},
    'Precip25': {
        'units': 'mm', 'func': functools.partial(PrecipY, percentile=75.)
    },
    'Precip10': {
        'units': 'mm', 'func': functools.partial(PrecipY, percentile=90.)
    },
    'Precip5': {
        'units': 'mm', 'func': functools.partial(PrecipY, percentile=95.)
    },
    'Precip1': {
        'units': 'mm', 'func': functools.partial(PrecipY, percentile=99.)
    }
}

# used by `SpatialPlot` object (`plot` module) - physical figure parameters
# for 3-hourly (8 panel) and 24-hour (1 panel) plots
FREQUENCY_DICT = {
    3: {
        'name': '3-hourly', 
        'figshape': (2, 4), 
        'figsize': (18, 9),
        'cbar': (0.9, 0.92, 0.13, 0.02, 0.745)
    },
    24: {
        'name': 'Daily', 
        'figshape': (1, 1), 
        'figsize': (9, 9),
        'cbar': (0.9, 0.92, 0.13, 0.04, 0.745)
    }
}
 
# conditional on any rain - used in the `data_composer` function in `compose`
# module when constructing the data for plotting.
CONDITIONALS = [False, True]

# locations and accompanying text to include in the plots, and text offsets in m
LOCS_TO_PLOT = [
    ('YPDN', (-12.415, 130.877), (6000, -6000)),  # Darwin airport
    ('YBTI', (-11.769, 130.620), (6000, 0)),  # Bathurst Island airport
    ('YBCR', (-13.056, 131.028), (6000, 0))  # Batchelor airfield
]
