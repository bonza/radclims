# About

`radclims` provides a Python library for producing spatial rainfall climatologies 
from raw .mdv radar files.

It was developed for the Gunn Point radar in Darwin, Australia, so several configuration 
parameters will need to be changed before using the package on other radars. These are
declared in the file `config.py`.

The package consists of two main sections:
- Code to clean, prepare, and process climatology data from raw .mdv radar files.
- Plotting functions to produce a set of default figures from the resulting data.

User intervention is mostly limited to the first dot point.

# Setting up

An installation script isn't included. Instead, just download the radclims package 
into the directory you want to work out of.

You will also need an existing mdv directory, which contains all of the mdv data to be 
incorporated in the results. Specifically, this directory should contain year directories,
which in turn contain day directories, which in turn contain .mdv files for each individual radar scan, named according to time of scan (hhmmss), for example:

`.../mdv/2019/20190101/235000.mdv`

In addition, appropriately named directories to house data after each processing step
should also be created. More on this this can be found in the relevant module documentation.

Once these paths are created they should be specified in `config.py`.

# Usage

Data processing and figure creation is a linear process. Simply work through the following
modules in the given order:

1. `mdv.py`: reads mdv files into `xarray.DataArray` objects according to day.
2. `prepare.py`: performs some interpolations on the data and derives approximate rainfall accumulations.
3. `stratify.py`: filters accumulation data based on month, or convective regime, or other user-specified time partitions.
4. `process.py`: computes probability of precipitation (PoPX) and precipitation percentiles (PrecipY).
5. `make_figures.py`: makes figures for all parameter permutations provided in `config.py`, and saves them as png files to a specified path.

Once this process is completed, there will be a folder containing a ton of figures. Download the climatologies viewer webpage (separate repo, `radclims_viewer`), point the webpage's javascript to this folder, and explore the results!
